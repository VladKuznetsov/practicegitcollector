package gitapi

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/imroc/req"
	"github.com/tomnomnom/linkheader"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

//
func getNewGitFromConfig(file string) (*Git, error) {
	g := new(Git)
	f, err := os.Open(file)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	bytes, err := ioutil.ReadAll(f)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	err = json.Unmarshal(bytes, g)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	if g.User.Email == "" || g.User.Password == "" {
		return nil, errors.New("Auth data is null")
	}
	g.BasicAuthToken = base64.StdEncoding.EncodeToString(
		[]byte(
			strings.Join([]string{g.User.Email, g.User.Password}, ":"),
		),
	)
	setBasicHeaderAndParams(g)
	return g, nil
}

//
func getNewGitFromBytes(bytes []byte) (*Git, error) {
	g := new(Git)
	err := json.Unmarshal(bytes, g)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	if g.User.Email == "" || g.User.Password == "" {
		return nil, errors.New("Auth data is null")
	}
	g.BasicAuthToken = base64.StdEncoding.EncodeToString(
		[]byte(
			strings.Join([]string{g.User.Email, g.User.Password}, ":"),
		),
	)
	setBasicHeaderAndParams(g)
	return g, nil
}

func setBasicHeaderAndParams(g *Git) {
	basicAuthHeader = req.Header{
		"Authorization": strings.Join([]string{"Basic", g.BasicAuthToken}, " "),
	}
}

func httpGetRequest(endpoints []string, params req.Param) (*http.Response, error) {
	allEndpoints := append([]string{Domain}, endpoints...)
	url := strings.Join(allEndpoints, "/")
	response, err := req.Get(url, basicAuthHeader, params)
	if err != nil {
		return response.Response(), err
	}
	if response.Response().StatusCode != http.StatusOK {
		log.Println("Bad Request =[code:'", response.Response().StatusCode, "', url:'", url, "']")
		return response.Response(), errors.New("Bad Request")
	}
	go log.Println("Successful Request=[code:'", response.Response().StatusCode, "', url:'", url, "']")
	return response.Response(), nil
}

// Парсинг заголовка Link
//
func parseHeaderLink(response *http.Response) ([]linkheader.Link, func(links []linkheader.Link, rel string) string, func(links []linkheader.Link, param string) string, error) {
	var (
		findByRel = func(links []linkheader.Link, rel string) string {
			for _, l := range links {
				if l.Rel == rel {
					return l.URL
				}
			}
			return ""
		}
		findByParam = func(links []linkheader.Link, param string) string {
			for _, l := range links {
				for key, val := range l.Params {
					if key == val {
						return l.URL
					}
				}
			}
			return ""
		}
	)
	header := response.Header.Get("Link")
	if header == "" {
		return nil, nil, nil, errors.New("Header Link . is nil")
	}
	links := linkheader.Parse(header)
	if len(links) == 0 {
		return nil, nil, nil, errors.New("Links is nil")
	}
	return links, findByRel, findByParam, nil
}

func getCountPagesFromHeaderLink(response *http.Response, rel, param string) (int, error) {
	links, findByRel, findByParam, err := parseHeaderLink(response)
	if err != nil {
		return -1, err
	}
	pageUrl := ""
	if rel != "" {
		pageUrl = findByRel(links, rel)
		if pageUrl == "" {
			return -1, errors.New("links did't contain url's page")
		}
	} else {
		if param != "" {
			pageUrl = findByParam(links, param)
			if pageUrl == "" {
				return -1, errors.New("links did't contain url's page")
			}
		} else {
			return -1, errors.New("links did't contain url's page")
		}
	}
	parseUrl, err := url.Parse(pageUrl)
	if err != nil {
		return -1, err
	}
	m, err := url.ParseQuery(parseUrl.RawQuery)
	if err != nil {
		return -1, err
	}
	count, err := strconv.Atoi(m["page"][0])
	if err != nil {
		return -1, err
	}
	return count, nil
}

// https://api.github.com/repos/<owner>/<name>
//
func gitGetRepo(name string, owner *User, param req.Param) (*Repo, *http.Response, error) {
	repository := new(Repo)
	response, err := httpGetRequest([]string{"repos", owner.Login, name}, param)
	if err != nil {
		return repository, response, err
	}
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("ERROR, func gitGetRepo(name string, owner *User) : ioutil.ReadAll : ", err, "")
		return repository, response, err
	}
	err = json.Unmarshal(bytes, repository)
	if err != nil {
		log.Println("ERROR, func gitGetRepo(name string, owner *User) : json.Unmarshal : ", err, "")
		return repository, response, err
	}
	return repository, response, nil
}

// https://api.github.com/repos/<owner>/<name>/issues
//
func gitListIssues(name string, owner *User, param req.Param) ([]*Issue, *http.Response, error) {
	issues := make([]*Issue, 0)
	response, err := httpGetRequest([]string{"repos", owner.Login, name, "issues"}, param)
	if err != nil {
		return issues, response, err
	}
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("ERROR, func gitListIssues(name string, owner *User) : ioutil.ReadAll : ", err, "")
		return issues, response, err
	}
	err = json.Unmarshal(bytes, &issues)
	if err != nil {
		log.Println("ERROR, func gitListIssues(name string, owner *User) : json.Unmarshal : ", err, "")
		return issues, response, err
	}
	return issues, response, nil
}

// https://api.github.com/repos/<owner>/<name>/pulls/<number>/commits
//
func gitListIssueCommits(name string, owner *User, number int, param req.Param) ([]*Commit, *http.Response, error) {
	commits := make([]*Commit, 0)
	response, err := httpGetRequest([]string{"repos", owner.Login, name, "pulls", strconv.Itoa(number), "commits"}, param)
	if err != nil {
		return commits, response, err
	}
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("ERROR, func gitListIssueCommits(name string, owner *User, number int) : ioutil.ReadAll : ", err, "")
		return commits, response, err
	}
	err = json.Unmarshal(bytes, &commits)
	if err != nil {
		log.Println("ERROR, func gitListIssueCommits(name string, owner *User, number int) : json.Unmarshal : ", err, "")
		return commits, response, err
	}
	return commits, response, nil
}

// https://api.github.com/repos/<owner>/<name>/commits
//
func gitListCommits(name string, owner *User, param req.Param) ([]*Commit, *http.Response, error) {
	commits := make([]*Commit, 0)
	response, err := httpGetRequest([]string{"repos", owner.Login, name, "commits"}, param)
	if err != nil {
		return commits, response, err
	}
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("ERROR, func gitListIssueCommits(name string, owner *User, number int) : ioutil.ReadAll : ", err, "")
		return commits, response, err
	}
	err = json.Unmarshal(bytes, &commits)
	if err != nil {
		log.Println("ERROR, func gitListIssueCommits(name string, owner *User, number int) : json.Unmarshal : ", err, "")
		return commits, response, err
	}
	return commits, response, nil
}
