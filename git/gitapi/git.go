package gitapi

import (
	"errors"
	"github.com/imroc/req"
	"log"
	"net/http"
	"strconv"
	"time"
)

const Domain = "https://api.github.com"

var basicAuthHeader req.Header

// Git : git-агент
type Git struct {
	*User          `json:"user"`
	BasicAuthToken string
	AuthStatus     bool
	Repos          []*Repo `json:"repos"`
}

// Repo : репозиторий
type Repo struct {
	Owner           *User  `json:"owner"`
	Name            string `json:"name"`
	NodeID          string `json:"node_id"`
	FullName        string `json:"full_name"`
	URL             string `json:"url"`
	ID              int    `json:"id"`
	CollectedIssues []*Issue
	Issues          []*Issue
	Commits         []*Commit
}

// Issue : задача
type Issue struct {
	ID      int       `json:"id"`
	Number  int       `json:"number"`
	NodeID  string    `json:"node_id"`
	URL     string    `json:"url"`
	Date    time.Time `json:"created_at"`
	Author  *User     `json:"user"`
	Message string    `json:"body"`
	Pulls   struct {
		URL string `json:"url"`
	} `json:"pull_request"`
	Commits []*Commit
}

// Commit : коммит
type Commit struct {
	NodeID string `json:"node_id"`
	Author *User  `json:"user"`
	Info   struct {
		Message  string `json:"message"`
		URL      string `json:"url"`
		Commiter struct {
			Email string    `json:"email"`
			Login string    `json:"name"`
			Date  time.Time `json:"date"`
		} `json:"committer"`
	} `json:"commit"`
}

// User : пользователь
type User struct {
	Email    string `json:"email"`
	Login    string `json:"login"`
	Password string `json:"password"`
	ID       int    `json:"id"`
	NodeID   string `json:"node_id"`
	URL      string `json:"url"`
}

//
//
//
//-----init's-----------------------------------------------------------------------------------------------------------
//
//
//

// NewGitFromConfig : Создаём нового git-агент из конфигов.
//
// Через него будут доступны запросы к github,
// независимо от настроенных конфигураций
//
// * file - путь до файла с конфигами.
//
func NewGitFromConfig(file string) (*Git, error) {
	g, err := getNewGitFromConfig(file)
	if err != nil {
		log.Println(",ERROR, func NewGitFromConfig(file string) : Configs error : ", err, "")
		return nil, err
	}
	response, err := g.Auth()
	if err != nil {
		log.Println(",ERROR, func NewGitFromConfig(file string) : Auth error : ", response, "")
		return nil, err
	}
	return g, err
}

// NewGitFromBytes : Создаём нового git-агент из запроса.
//
// Через него будут доступны запросы к github,
// независимо от настроенных конфигураций
//
// * bytes - request JSON.
//
func NewGitFromBytes(bytes []byte) (*Git, error) {
	g, err := getNewGitFromBytes(bytes)
	if err != nil {
		log.Println(",ERROR, func NewGitFromConfig(file string) : Configs error : ", err, "")
		return nil, err
	}
	response, err := g.Auth()
	if err != nil {
		log.Println(",ERROR, func NewGitFromConfig(file string) : Auth error : ", response, "")
		return nil, err
	}
	return g, err
}

//
//
//
//-----func's-----------------------------------------------------------------------------------------------------------
//
//
//

// Auth : Авторизация.
//
// https://api.github.com/user
//
// * <name> - ...
//
func (g *Git) Auth() (*http.Response, error) {
	if g == nil || g.BasicAuthToken == "" {
		return nil, errors.New("Auth data is null")
	}
	response, err := httpGetRequest([]string{"user"}, nil)
	if err != nil {
		g.AuthStatus = false
		return response, err
	}
	g.AuthStatus = true
	return response, nil
}

// AddRepo : Добавить репозиторий.
//
// https://api.github.com/repos/<owner>/<name>
//
// * <name> - ...
//
func (g *Git) AddRepo(name, owner string) (*Repo, error) {
	if !g.AuthStatus {
		log.Println("ERROR, func (g *Git) GetRepo(name string) : Auth error")
		return nil, errors.New("Auth is false")
	}
	if len(g.Repos) == 0 {
		g.Repos = make([]*Repo, 0)
	}
	repo, _, err := gitGetRepo(name, &User{
		Login: owner,
	}, nil)
	if err != nil {
		log.Println("ERROR, func UpdateRepo(name string, owner *User) : ", err, "")
		return nil, errors.New("Repo isn't exist")
	}
	if repo == nil {
		log.Println("ERROR, func UpdateRepo(name string, owner *User) : Repo isn't exist")
		return nil, errors.New("Repo isn't exist")
	}
	g.Repos = append(g.Repos, repo)
	return g.Repos[len(g.Repos)-1], nil
}

// GetRepo : Вернуть репозиторий.
//
// https://api.github.com/repos/<owner>/<name>
//
// * <name> - ...
//
func (g *Git) GetRepo(name string) (*Repo, error) {
	if !g.AuthStatus {
		log.Println("ERROR, func (g *Git) GetRepo(name string) : Auth error")
		return nil, errors.New("Auth is false")
	}
	if len(g.Repos) == 0 {
		g.Repos = make([]*Repo, 0)
	}
	for _, repo := range g.Repos {
		if repo.Name == name {
			if repo.NodeID == "" && repo.Owner != nil && repo.Owner.Login != "" {
				update, _, err := gitGetRepo(name, repo.Owner, nil)
				if err != nil {
					log.Println("ERROR, func UpdateRepo(name string, owner *User) : ", err, "")
					return nil, errors.New("Repo isn't exist")
				}
				if update == nil {
					log.Println("ERROR, func UpdateRepo(name string, owner *User) : Repo isn't exist")
					return nil, errors.New("Repo isn't exist")
				}
				*repo = *update
			}
			return repo, nil
		}
	}
	return nil, errors.New("Repo isn't exist")
}

// IssueCountPages : вернуть кол-во страниц с issue
//
// <url>
//
// * <name> - ...
//
func (r *Repo) IssueCountPages() int {
	_, response, err := gitListIssues(r.Name, r.Owner, nil)
	if err != nil {
		log.Println("ERROR, func (r *Repo) IssueCountPages() ", err)
		return -1
	}
	countIssuePages, err := getCountPagesFromHeaderLink(response, "last", "")
	if err != nil {
		log.Println("ERROR, func (r *Repo) IssueCountPages() : ", err)
		return -1
	}
	return countIssuePages
}

// CommitsCountPages : вернуть кол-во страниц с commit
//
// <url>
//
// * <name> - ...
//
func (r *Repo) CommitsCountPages() int {
	_, response, err := gitListCommits(r.Name, r.Owner, nil)
	if err != nil {
		log.Println("ERROR, func (r *Repo) CommitsCountPages() ", err)
		return -1
	}
	countCommitsPages, err := getCountPagesFromHeaderLink(response, "last", "")
	if err != nil {
		log.Println("ERROR, func (r *Repo) CommitsCountPages() : ", err)
		return -1
	}
	return countCommitsPages
}

//
//
//
//-----Вернуть списки---------------------------------------------------------------------------------------------------
//
//
//

// ListIssues : списки issue
//
// https://api.github.com/repos/<owner>/<name>/issues
//
// * p ...req.Param - параметр задаваем, если
//        необходимо найти на конкретной странице.
//        HTTP Params : <url>?page=<number>
//
func (r *Repo) ListIssues(p ...req.Param) ([]*Issue, *http.Response, error) {
	params := req.Param{}
	if len(p) != 0 {
		params = p[0]
	}
	issues, response, err := gitListIssues(r.Name, r.Owner, params)
	if err != nil || len(issues) == 0 {
		log.Println("ERROR, func (r *Repo) CollectIssues() : bad req.")
		return issues, response, errors.New("bad req. ")
	}
	go log.Println("OK, func (r *Repo) ListIssues(p ...req.Param) : issues was collect")
	return issues, response, nil
}

// ListCommits : списки commits
//
// https://api.github.com/repos/<owner>/<name>/commits
//
// * p ...req.Param - параметр задаваем, если
//        необходимо найти на конкретной странице.
//        HTTP Params : <url>?page=<number>
//
func (r *Repo) ListCommits(p ...req.Param) ([]*Commit, *http.Response, error) {
	params := req.Param{}
	if len(p) != 0 {
		params = p[0]
	}
	commits, response, err := gitListCommits(r.Name, r.Owner, params)
	if err != nil || len(commits) == 0 {
		log.Println("ERROR, func (r *Repo) CollectIssues() : bad req.")
		return commits, response, errors.New("bad req. ")
	}
	go log.Println("OK, func (r *Repo) ListCommits(p ...req.Param) : commits was collect")
	return commits, response, nil
}

// ListIssueCommits : списки commits у конкретного issue.
//
// https://api.github.com/repos/<owner>/<name>/pulls/<number>/commits
//
// * number int - номер issue
//
func (r *Repo) ListIssueCommits(number int) ([]*Commit, *http.Response, error) {
	commits, response, err := gitListIssueCommits(r.Name, r.Owner, number, nil)
	if err != nil || len(commits) == 0 {
		log.Println("ERROR, func (r *Repo) CollectIssues() : 'commits' is 'nil'")
		return commits, response, err
	}
	go log.Println("OK, func (r *Repo) ListIssueCommits(number int) : list commits was collect")
	return commits, response, nil
}

// CollectIssues : Собираем все issue по найденным страницам.
//
// https://api.github.com/repos/<owner>/<name>/issues?page=<number>
//
// * <name> - ...
//
func (r *Repo) ListIssuesFromPages() ([]*Issue, error) {
	var (
		list            = make([]*Issue, 0)
		countIssuePages = r.IssueCountPages()
	)
	if countIssuePages == -1 {
		log.Println("WARNING, func (r *Repo) ListIssuesFromPages() , in repo '", r.Name, "' pages not found.")
		countIssuePages = 100
	}
	for numPage := 1; numPage <= countIssuePages; numPage++ {
		issues, _, _ := r.ListIssues(req.Param{
			"page": strconv.Itoa(numPage),
		})
		if len(issues) == 0 {
			log.Println("WARNING, func (r *Repo) ListIssuesFromPages() , in repo '", r.Name, "' found last page.")
			if len(list) == 0 {
				return list, errors.New("For repo. pages not found ")
			} else {
				return list, nil
			}
		}
		list = append(list, issues...)
	}
	go log.Println("OK, func (r *Repo) ListIssuesFromPages() : list issue was collect")
	return list, nil
}

// ListCommitsFromPages : Собираем все commit по найденным страницам.
//
// https://api.github.com/repos/<owner>/<name>/commits?page=<number>
//
// * <name> - ...
//
func (r *Repo) ListCommitsFromPages() ([]*Commit, error) {
	var (
		list             = make([]*Commit, 0)
		countCommitPages = r.CommitsCountPages()
	)
	if countCommitPages == -1 {
		log.Println("WARNING, func (r *Repo) ListCommitsFromPages() , in repo '", r.Name, "' pages not found.")
		countCommitPages = 100
	}
	for numPage := 1; numPage <= countCommitPages; numPage++ {
		commits, _, _ := r.ListCommits(req.Param{
			"page": strconv.Itoa(numPage),
		})
		if len(commits) == 0 {
			log.Println("WARNING, func (r *Repo) ListCommitsFromPages() , in repo '", r.Name, "' found last page.")
			if len(list) == 0 {
				return list, errors.New("For repo. pages not found ")
			} else {
				return list, nil
			}
		}
		list = append(list, commits...)
	}
	go log.Println("OK,func (r *Repo) ListCommitsFromPages() : list commits was collect")
	return list, nil
}

//
//
//
//-----Коллекции--------------------------------------------------------------------------------------------------------
//
//
//

// CollectIssues : Собираем все issue, затем commits у конкретного issue.
//
// <url>
//
// * <name> - ...
//
func (r *Repo) CollectIssues() {
	if len(r.CollectedIssues) == 0 {
		r.CollectedIssues = make([]*Issue, 0)
	}
	issues, err := r.ListIssuesFromPages()
	if err != nil {
		log.Println("ERROR, func (r *Repo) CollectIssues() : ", err)
		return
	}
	for _, issue := range issues {
		if issue.Pulls.URL == "" {
			log.Println("ERROR, func (r *Repo) CollectIssues() : ", issue.URL, " without commits by issue number : ", issue.Number)
			continue
		}
		commits, _, err := r.ListIssueCommits(issue.Number)
		if err != nil {
			log.Println("ERROR, func (r *Repo) CollectIssues() : ", err)
			continue
		}
		issue.Commits = commits
		r.CollectedIssues = append(r.CollectedIssues, issue)
	}
	go log.Println("OK, func (r *Repo) CollectIssues() : issues and them commits was collect")
}
