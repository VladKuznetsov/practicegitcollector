package gitapi

import (
	"log"
	"testing"
)

func TestRepo(t *testing.T) {
	name := "gin"
	owner := &User{
		Login: "gin-gonic",
	}
	repo, _, err := gitGetRepo(name, owner, nil)
	if err != nil {
		log.Println("ERROR, Test : func TestRepo(t *testing.T), ", err, "")
		return
	}
	if repo == nil {
		log.Println("ERROR, Test : func TestRepo(t *testing.T), 'repo' value equal 'nil'")
		return
	}
	log.Println("OK, Test : func TestRepo(t *testing.T)")
}

func TestListIssues(t *testing.T) {
	name := "gin"
	owner := &User{
		Login: "gin-gonic",
	}
	issues, _, err := gitListIssues(name, owner, nil)
	if err != nil {
		log.Println("ERROR, Test : func TestListIssues(t *testing.T), ", err, "")
		return
	}
	if issues == nil {
		log.Println("ERROR, Test : func TestListIssues(t *testing.T), 'issues' value equal 'nil'")
		return
	}
	if len(issues) == 0 {
		log.Println("ERROR, Test : func TestListIssues(t *testing.T), 'issues' arr. len. equal 0")
		return
	}
	log.Println("OK, Test : func TestListIssues(t *testing.T)")
}

func TestListIssueCommits(t *testing.T) {
	name := "gin"
	owner := &User{
		Login: "gin-gonic",
	}
	number := 2441
	commits, _, err := gitListIssueCommits(name, owner, number, nil)
	if err != nil {
		log.Println("ERROR, Test : func TestListIssueCommits(t *testing.T), ", err, "")
		return
	}
	if commits == nil {
		log.Println("ERROR, Test : func TestListIssueCommits(t *testing.T), 'issues' value equal 'nil'")
		return
	}
	if len(commits) == 0 {
		log.Println("ERROR, Test : func TestListIssueCommits(t *testing.T), 'issues' arr. len. equal 0")
		return
	}
	log.Println("OK, Test : func TestListIssueCommits(t *testing.T)")
}

func TestCollected(t *testing.T) {
	git, err := NewGitFromConfig("C:/Users/Vlad/Desktop/Предметы/Практика/GitHubAnalyser/config.json")
	if err != nil {
		log.Println("ERROR, Test : func TestCollected(t *testing.T) : NewGitFromConfig() , ", err, "")
		return
	}
	repo, err := git.GetRepo("gin")
	if err != nil {
		log.Println("ERROR, Test : func TestCollected(t *testing.T) : GetRepo() 1 , ", err, "")
		return
	}
	repo.CollectIssues()
	checkout, err := git.GetRepo("gin")
	if err != nil {
		log.Println("ERROR, Test : func TestCollected(t *testing.T) : GetRepo() 2, ", err, "")
		return
	}
	for _, r := range git.Repos {
		if r.Name == "gin" {
			if &(*checkout) != &(*repo) {
				log.Println("ERROR, Test : func TestCollected(t *testing.T) : &checkout = ", &(*checkout), ", &repo = ", &(*repo), "")
				return
			}
			if &(*r) != &(*repo) {
				log.Println("ERROR, Test : func TestCollected(t *testing.T) : &(*r) = ", &(*r), ", &repo = ", &(*repo), "")
				return
			}
			if r != repo {
				log.Println("ERROR, Test : func TestCollected(t *testing.T) : r = ", *r, ", repo = ", *repo, "")
				return
			} else {
				test := &Repo{}
				if r == test {
					log.Println("ERROR, Test : func TestCollected(t *testing.T) : r = ", *r, ", test = ", *test, "")
					return
				}
			}
			break
		}
	}
	log.Println("OK, Test : func TestCollected(t *testing.T)")
}
