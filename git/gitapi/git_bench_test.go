package gitapi

import (
	"testing"
)

// go test git_bench_test.go -benchmem -bench=.

func BenchmarkCollected(b *testing.B) {
	for i := 0; i < b.N; i++ {
		git, _ := NewGitFromConfig("C:/Users/Vlad/Desktop/Предметы/Практика/GitHubAnalyser/config.json")
		repo, _ := git.GetRepo("gin")
		repo.CollectIssues()
	}
}
