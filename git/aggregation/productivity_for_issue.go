package aggregation

import (
	"../../git/gitapi"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"time"
)

type setting int

const (
	SettingBasic setting = 0
	SettingExact setting = 1
)

// IssueWorkersProductivity : сборщик данных
type IssueWorkersProductivity struct {
	repo  *gitapi.Repo
	Name  string               `json:"name"`
	Exact []*IWPAnalysisParams `json:"exact"`
	Basic []*IWPAnalysisParams `json:"basic"`
}

// IWPAnalysisParams : параметры анализа
type IWPAnalysisParams struct {
	IssueNumber  int     `json:"issue_number"`
	HalfMonth    int     `json:"half_month"`
	Day          int     `json:"day"`
	DayPercent   float64 `json:"day_percent"`
	Productivity float64 `json:"productivity"`
}

type IWPStorage struct {
	IWPs []*IssueWorkersProductivity `json:"storage"`
}

//
//
//
//-----init's-----------------------------------------------------------------------------------------------------------
//
//
//

// NewIWP : создаёт нового агента.
//
// * r *gitapi.Repo- репозиторий.
//
func NewIWP(r ...*gitapi.Repo) (*IssueWorkersProductivity, error) {
	iwp := new(IssueWorkersProductivity)
	iwp.Basic = make([]*IWPAnalysisParams, 0)
	iwp.Exact = make([]*IWPAnalysisParams, 0)
	if len(r) != 0 {
		if r[0] == nil || len(r[0].CollectedIssues) == 0 {
			log.Println("ERROR, func NewIWP(r *gitapi.Repo), some data is nil")
			return nil, errors.New("some data is nil ")
		}
		iwp.repo = r[0]
		iwp.Name = r[0].Name
	}
	return iwp, nil
}

func NewIWPStorage() *IWPStorage {
	storage := new(IWPStorage)
	storage.IWPs = make([]*IssueWorkersProductivity, 0)
	return storage
}

//
//
//
//-----private's---------------------------------------------------------------------------------------------------------
//
//
//

// calcProductivity : считает продуктивность.
//
// * denumenator - количество часов.
// * count - количество коммитов
//
func (iwp *IssueWorkersProductivity) calcProductivity(denumenator, count int) float64 {
	return float64(count) / float64(denumenator)
}

// getProductivity : возвращает продуктивность для
//                 для конкретного issue измеренная в процентах.
//
// * issueDate *time.Time - дата назначения issue.
// * commits []*gitapi.Commit - все коммиты issue.
// * s setting - настройка анализа:
//               - базовый
//               - расширенный
//
func (iwp *IssueWorkersProductivity) getProductivity(s setting, commits []*gitapi.Commit, issueDate *time.Time) (float64, error) {
	productivity := 0.0
	switch s {
	case SettingBasic:
		productivity = iwp.calcProductivity(720, len(commits))
		break
	case SettingExact:
		last, err := iwp.getLastCommit(issueDate, commits)
		if err != nil {
			return productivity, err
		}
		difference, err := getDifferenceHours(issueDate, &last)
		if err != nil {
			return productivity, errors.New("Error search difference in dates ")
		}
		if difference == 0 {
			difference = 1
		}
		productivity = iwp.calcProductivity(difference*720, len(commits))
		break
	default:
		return productivity, errors.New("Setting isn't exist")
	}
	return productivity * 100, nil
}

// getLastCommit : поиск последнего коммита за период
//                 от даты назначения issue до месяца вперед
//                 от даты назначения issue.
//
// * issueDate *time.Time - дата назначения issue.
// * commits []*gitapi.Commit - все коммиты issue.
//
func (iwp *IssueWorkersProductivity) getLastCommit(issueDate *time.Time, commits []*gitapi.Commit) (time.Time, error) {
	var (
		period = issueDate.Add(24 * time.Hour * 30)
		last   = commits[len(commits)-1].Info.Commiter.Date
		check  = func(issue, last, period *time.Time) (bool, error) {
			later, err := isLater(issue, last, period)
			if err != nil {
				return false, err
			}
			if later {
				return true, nil
			}
			return false, nil
		}
	)
	later, err := check(issueDate, &last, &period)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) getLastCommit(issueDate *time.Time, commits []*gitapi.Commit) , ", err)
		return last, err
	}
	if !later {
		return last, nil
	}
	for _, commit := range commits {
		later, err := check(issueDate, &commit.Info.Commiter.Date, &period)
		if err != nil {
			log.Println("ERROR, func (iwp *IssueWorkersProductivity) getLastCommit(issueDate *time.Time, commits []*gitapi.Commit) , ", err)
			return last, err
		}
		if !later {
			return last, nil
		}
	}
	return time.Time{}, errors.New("getLastCommit: Date isn't later exist ")
}

//
//
//
//-----public's---------------------------------------------------------------------------------------------------------
//
//
//

func (storage *IWPStorage) Add(iwp *IssueWorkersProductivity) error {
	if iwp == nil {
		return errors.New("IWP is nil")
	}
	if len(iwp.Exact) == 0 && len(iwp.Basic) == 0 {
		return errors.New("IWP data is empty")
	}
	storage.IWPs = append(storage.IWPs, iwp)
	return nil
}

func (storage *IWPStorage) SaveJSONFile(file string) error {
	bytes, err := json.Marshal(storage)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) JSONSerialize(), ", err)
		return err
	}
	err = ioutil.WriteFile(file, bytes, 0644)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) JSONSerialize(), ", err)
		return err
	}
	return nil
}

// NewRepo : Добавить новый репозиторий.
//
// * r *gitapi.Repo - репозиторий
//
func (iwp *IssueWorkersProductivity) NewRepo(r *gitapi.Repo) error {
	if r == nil || len(r.CollectedIssues) == 0 {
		log.Println("ERROR, func NewIWP(r *gitapi.Repo), some data is nil")
		return errors.New("some data is nil ")
	}
	iwp.repo = r
	iwp.Name = r.Name
	return nil
}

// BasicIssueAnalysis : базовый анализ конкретного issue.
//
// * issue *gitapi.Issue - issue
//
func (iwp *IssueWorkersProductivity) BasicIssueAnalysis(issue *gitapi.Issue) (*IWPAnalysisParams, error) {
	halfMonth, err := getHalfOfMonth(&issue.Date)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), ", err, ", issue :", issue.Number)
		return nil, err
	}
	params := new(IWPAnalysisParams)
	productivity, err := iwp.getProductivity(SettingBasic, issue.Commits, &issue.Date)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), ", err, ", issue :", issue.Number)
		return nil, err
	}
	params.Productivity = productivity
	params.HalfMonth = halfMonth
	params.IssueNumber = issue.Number
	params.Day = issue.Date.Day()
	params.DayPercent = (float64(issue.Date.Day()) / float64(30)) * float64(100)
	return params, nil
}

// ExactIssueAnalysis : расширенный анализ конкретного issue.
//
// * issue *gitapi.Issue - issue
//
func (iwp *IssueWorkersProductivity) ExactIssueAnalysis(issue *gitapi.Issue) (*IWPAnalysisParams, error) {
	halfMonth, err := getHalfOfMonth(&issue.Date)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), ", err, ", issue :", issue.Number)
		return nil, err
	}
	params := new(IWPAnalysisParams)
	productivity, err := iwp.getProductivity(SettingExact, issue.Commits, &issue.Date)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), ", err, ", issue :", issue.Number)
		return nil, err
	}
	params.Productivity = productivity
	params.HalfMonth = halfMonth
	params.IssueNumber = issue.Number
	params.Day = issue.Date.Day()
	params.DayPercent = (float64(issue.Date.Day()) / float64(30)) * float64(100)
	return params, nil
}

// Analysis : полный анализ репозитория.
//
// * s setting - настройка анализа:
//               - базовый
//               - расширенный
//
func (iwp *IssueWorkersProductivity) Analysis(s setting) error {
	if len(iwp.Basic) == 0 {
		iwp.Basic = make([]*IWPAnalysisParams, 0)
	}
	if len(iwp.Exact) == 0 {
		iwp.Exact = make([]*IWPAnalysisParams, 0)
	}
	for _, issue := range iwp.repo.CollectedIssues {
		switch s {
		case SettingBasic:
			analysis, err := iwp.BasicIssueAnalysis(issue)
			if err != nil {
				log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), ", err, ", issue :", issue.Number)
				return err
			}
			iwp.Basic = append(iwp.Basic, analysis)
			break
		case SettingExact:
			analysis, err := iwp.ExactIssueAnalysis(issue)
			if err != nil {
				log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), ", err, ", issue :", issue.Number)
				return err
			}
			iwp.Exact = append(iwp.Exact, analysis)
			break
		default:
			log.Println("ERROR, func (iwp *IssueWorkersProductivity) Analysis(s setting), setting isn't exist")
			return errors.New("Setting isn't exist ")
		}
	}
	return nil
}

// JSONSerialize : сериализация.
//
// * <name> - ...
//
func (iwp *IssueWorkersProductivity) JSONSerialize() ([]byte, error) {
	bytes, err := json.Marshal(iwp)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) JSONSerialize(), ", err)
		return nil, err
	}
	return bytes, nil
}

func (iwp *IssueWorkersProductivity) SaveJSONFile(file string) error {
	bytes, err := json.Marshal(iwp)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) JSONSerialize(), ", err)
		return err
	}
	err = ioutil.WriteFile(file, bytes, 0644)
	if err != nil {
		log.Println("ERROR, func (iwp *IssueWorkersProductivity) JSONSerialize(), ", err)
		return err
	}
	return nil
}

//
//
//
//-----func's-----------------------------------------------------------------------------------------------------------
//
//
//

func getDifferenceHours(start, later *time.Time) (int, error) {
	if start == nil || later == nil {
		return -1, errors.New("Data is nil ")
	}
	duration := int(later.Sub(*start).Hours())
	if duration < 0 {
		duration = 1
	}
	return duration, nil
}

func getHalfOfMonth(date *time.Time) (int, error) {
	if date == nil {
		return -1, errors.New("Data is nil ")
	}
	value := -1
	if date.Day() <= 15 {
		value = 1
	}
	if date.Day() > 15 {
		value = 2
	}
	return value, nil
}

func isLater(start, later, period *time.Time) (bool, error) {
	if start == nil || later == nil || period == nil {
		return false, errors.New("Data is nil ")
	}
	// isLater := 1 && 0 ->
	isLater := period.After(*start) && period.Before(*later)
	return isLater, nil
}
