import json
import os
import sys
import pandas
import matplotlib.pyplot as plt
import math
import numpy as np
from scipy import stats

"""
def corr_pearson(data_frame):

Корреляционный анализ методом Пирсона.
"""


def corr_pearson(data_frame):
    cor = data_frame.corr(method='pearson')
    return cor


def corr_biserial(y, x):
    cor = stats.pointbiserialr(x, y)
    return cor


"""
def get_data_frame(y, x):

Ковариация исходных данных.
"""


def covariance(data_frame):
    cov = data_frame.cov()
    d_x = np.var(data_frame['Productivity'])
    d_y = np.var(data_frame['Month Half'])
    print('Дисперсия Productivity : ', d_x)
    print('Дисперсия Month Half : ', d_y, "\n")
    return cov


"""
def turn_over_data(data_frame):

Переверни к виду:

   | первая половина месяца | вторая половина месяца
___|________________________|_______________________
   |                        |                       
   |        0.78212         |           0.03455             
___|________________________|_______________________
   |                        |                       
   |        0.12            |            0.0002            
___|________________________|_______________________
   |                        |                       
   |                        |                        
   |...                     | ...
"""


def turn_over_data(data_frame):
    first_half = []
    second_half = []
    for index, row in data_frame.iterrows():
        if row['Month Half'] <= 50.0:
            first_half.append(row['Productivity'])
        else:
            second_half.append(row['Productivity'])
    data_frame = pandas.DataFrame(list(zip(first_half, second_half)), columns=['First Half', 'Second Half'])
    d_first = np.var(first_half)
    d_second = np.var(second_half)
    print('Дисперсия First Half : ', d_first)
    print('Дисперсия Second Half : ', d_second, "\n")
    return data_frame, first_half, second_half


"""
def get_data_frame(y, x):

Верни DataFrame из исходных данных.
"""


def get_data_frame(y, x):
    data_frame = pandas.DataFrame(list(zip(y, x)), columns=['Month Half', 'Productivity'])
    return data_frame


"""
def decode_productivity_day_percent(file):

Читаем Json.

 |        Часть месяца             |           Продуктивность        |
_|_________________________________|_________________________________| 
 | Интревальная перемеременная (%) | Интревальная перемеременная (%) |
 |                                 |                                 |
"""


def decode_productivity_day_percent(file):
    x = []
    y = []
    with open(file) as json_file:
        data = json.load(json_file)
        for repo in data['storage']:
            for issue in repo['exact']:
                x.append(issue['productivity'])
                y.append(issue['day_percent'])
    if len(x) != len(y):
        print("len x not equal len y")
        os._exit(2)
    return y, x


"""
def decode_productivity_day_percent(file):

Читаем Json.

 |        Часть месяца             |           Продуктивность        |
_|_________________________________|_________________________________| 
 | Дихтомическая перемен. (1 or 2) | Интревальная перемеременная (%) |
 |                                 |                                 |
"""


def decode_productivity_half_month(file):
    x = []
    y = []
    with open(file) as json_file:
        data = json.load(json_file)
        for repo in data['storage']:
            for issue in repo['exact']:
                x.append(issue['productivity'])
                y.append(issue['half_month'])
    if len(x) != len(y):
        print("len x not equal len y")
        os._exit(2)
    return y, x


"""
def analysis_cor_pearson(file):

Корреляционный анализ. Метод Пирсона
"""


def analysis_cor_pearson(file):
    print(
        "\n\n-----------------------------------------------------------------------------------------------------------")
    print('\033[1m\033[4mКорреляционный анализ (интервал. -> интервал.) :\033[0m')
    y, x = decode_productivity_day_percent(file)
    data_frame = get_data_frame(y, x)
    print('\n\033[1m\033[4mКовариционная матрица :\033[0m\n')
    cov = covariance(data_frame)
    print(cov)
    print('\n\033[1m\033[4mМетод Пирсона :\033[0m\n')
    cor = corr_pearson(data_frame)
    print(cor)
    return data_frame


"""
def analysis_cor_biserial(file):

Корреляционный анализ. Метод Бесериальный
"""


def analysis_cor_biserial(file):
    print(
        "\n\n-----------------------------------------------------------------------------------------------------------")
    print('\033[1m\033[4mКорреляционный анализ (Дихтом. -> интервал.) :\033[0m')
    y, x = decode_productivity_half_month(file)
    data_frame = get_data_frame(y, x)
    print('\n\033[1m\033[4mКовариционная матрица :\033[0m\n')
    cov = covariance(data_frame)
    print(cov)
    print('\n\033[1m\033[4mМетод Бесериальный :\033[0m\n')
    cor = corr_biserial(y, x)
    print(cor)

    return data_frame


def t_test(data_frame):
    frame, first_half, second_half = turn_over_data(data_frame)
    t = stats.ttest_ind(first_half, second_half)
    max_f = max(first_half)
    max_s = max(second_half)
    print(max_s)
    print(max_f)
    first_len = len(first_half)
    second_len = len(second_half)
    x = []
    if max_f > max_s:
        plt.ylim(top=max_f + max_f)
    else:
        plt.ylim(top=max_s + max_s)
    if first_len > second_len:
        for i in range(first_len):
            if i >= second_len:
                second_half.append(0)
            x.append(i)
    else:
        for i in range(second_len):
            if i >= first_len:
                first_half.append(0)
            x.append(i)
    plt.plot(x, first_half, label='First half')
    plt.plot(x, second_half, label='Second half')
    plt.legend()
    plt.show()
    return t


def main():
    print("PY start")
    if len(sys.argv) == 1:
        file = "C:\GoRepositories\practicegitcollector\data\data.json"
        data_frame_pearson = analysis_cor_pearson(file)
        print(data_frame_pearson)
        data_frame_biserial = analysis_cor_biserial(file)
        print(data_frame_biserial)
        t = t_test(data_frame_pearson)
        print(t)
    else:
        analysis_cor_pearson(sys.argv[1])


if __name__ == '__main__':
    main()
