package main

import (
	"./git/aggregation"
	"./git/gitapi"
	"log"
	"os"
	"os/exec"
	_ "os/exec"
)

func main() {
	log.Println("App is started")

	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	dataFile := path + "/data/data.json"

	git, err := gitapi.NewGitFromConfig(path + "/config.json")
	if err != nil {
		log.Println("ERROR, func main(), ", err)
		return
	}
	err = AggregateDataFromConfig(git, dataFile)
	if err != nil {
		log.Println("ERROR, func main(), ", err)
		return
	}

	err = RunningPython(dataFile, path)
	if err != nil {
		return
	}
}

func AggregateDataFromConfig(g *gitapi.Git, dataFile string) error {
	storage := aggregation.NewIWPStorage()
	for _, repo := range g.Repos {
		r, err := g.GetRepo(repo.Name)
		if err != nil {
			log.Println("ERROR, func AggregateDataFromConfig(g *gitapi.Git), ", err)
		}
		r.CollectIssues()
		analyser, err := aggregation.NewIWP(r)
		if err != nil {
			log.Println("ERROR, func AggregateDataFromConfig(g *gitapi.Git), ", err)
			continue
		}
		err = analyser.Analysis(aggregation.SettingExact)
		if err != nil {
			log.Println("ERROR, func AggregateDataFromConfig(g *gitapi.Git), ", err)
		}
		err = storage.Add(analyser)
		if err != nil {
			log.Println("ERROR, func AggregateDataFromConfig(g *gitapi.Git), ", err)
		}
	}
	err := storage.SaveJSONFile(dataFile)
	if err != nil {
		log.Println("ERROR, func AggregateDataFromConfig(g *gitapi.Git), ", err)
	}
	return err
}

func RunningPython(data, dir string) error {
	cmd := exec.Command(dir+"/py/venv/Scripts/python.exe", dir+"/py/main.py", data)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		log.Println("ERROR, cmd.Run() , ", err)
		return err
	}
	return nil
}
